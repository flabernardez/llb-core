<?php
/*
 * Plugin Name: LLB CORE Support
 * Plugin URI: https://luislobelda.com/
 * Description: Registro soporte MEDIA y CPTs
 * Version: 1.0
 * Author: Flavia Bernárdez
 * Author URI: https://flaviabernardez.com/
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

/* Files Support */
add_filter('upload_mimes', 'fla_soporte_files');
function fla_soporte_files($mimes = array()){

	$mimes['svg'] = 'image/svg+xml';
	$mimes['zip'] = 'application/zip';
	return $mimes;

}

/* Remove images link */
add_action('admin_init', 'wpb_imagelink_setup', 10);
function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );

	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}

/* Remove CSS Facet WP */
add_filter( 'facetwp_assets', function( $assets ) {
	unset( $assets['front.css'] );
	return $assets;
});

// Register Custom Crítica
add_action( 'init', 'llb_register_criticas', 0 );
function llb_register_criticas() {

	$labels = array(
		'name'                  => _x( 'Críticas', 'Crítica General Name', 'taurus-theme' ),
		'singular_name'         => _x( 'Crítica', 'Crítica Singular Name', 'taurus-theme' ),
		'menu_name'             => __( 'Críticas', 'taurus-theme' ),
		'name_admin_bar'        => __( 'Crítica', 'taurus-theme' ),
		'archives'              => __( 'Archivo de críticas', 'taurus-theme' ),
		'attributes'            => __( 'Atributos', 'taurus-theme' ),
		'parent_item_colon'     => __( 'Crítica padre:', 'taurus-theme' ),
		'all_items'             => __( 'Todas las críticas', 'taurus-theme' ),
		'add_new_item'          => __( 'Añadir nuevo crítica', 'taurus-theme' ),
		'add_new'               => __( 'Añadir nuevo', 'taurus-theme' ),
		'new_item'              => __( 'Nueva crítica', 'taurus-theme' ),
		'edit_item'             => __( 'Editar crítica', 'taurus-theme' ),
		'update_item'           => __( 'Actualizar crítica', 'taurus-theme' ),
		'view_item'             => __( 'Ver crítica', 'taurus-theme' ),
		'view_items'            => __( 'Ver críticas', 'taurus-theme' ),
		'search_items'          => __( 'Buscar crítica', 'taurus-theme' ),
		'not_found'             => __( 'No encontrada', 'taurus-theme' ),
		'not_found_in_trash'    => __( 'No encontrada en la papelera', 'taurus-theme' ),
		'featured_image'        => __( 'Imagen destacada', 'taurus-theme' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'taurus-theme' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'taurus-theme' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'taurus-theme' ),
		'insert_into_item'      => __( 'Insertar en crítica', 'taurus-theme' ),
		'uploaded_to_this_item' => __( 'Subido a esta crítica', 'taurus-theme' ),
		'items_list'            => __( 'Lista de críticas', 'taurus-theme' ),
		'items_list_navigation' => __( 'Navegación de la lista de críticas', 'taurus-theme' ),
		'filter_items_list'     => __( 'Filtrar lista de críticas', 'taurus-theme' ),
	);
	$args = array(
		'label'                    => __( 'Crítica', 'taurus-theme' ),
		'description'              => __( 'Descripción de crítica', 'taurus-theme' ),
		'labels'                   => $labels,
		'supports'                 => array( 'title','editor','author','excerpt','revisions','comments','thumbnail'),
		'taxonomies'               => array( 'tipo' ),
		'hierarchical'             => false,
		'public'                   => true,
		'show_ui'                  => true,
		'show_in_menu'             => true,
		'menu_position'            => 5,
		'show_in_admin_bar'        => true,
		'show_in_nav_menus'        => true,
		'can_export'               => true,
		'has_archive'              => 'criticas',
		'rewrite'                  => array( 'slug' => 'critica', 'with_front' => true ),
		'exclude_from_search'      => false,
		'publicly_queryable'       => true,
		'capability_type'          => 'page',
		'menu_icon'                => 'dashicons-testimonial',
	);

	register_post_type( 'criticas', $args );


	//creamos las taxonomías del custom post type
	$etiquetas_categoria = array(
		'name'              => _x( 'Tipo', 'nombre general' , 'taurus-theme' ),
		'singular_name'     => _x( 'Tipo de película', 'nombre singular' , 'taurus-theme' ),
		'search_items'      => __( 'Buscar tipo de película', 'taurus-theme'  ),
		'all_items'         => __( 'Todos los tipos de película', 'taurus-theme'  ),
		'parent_item'       => __( 'Tipo de película padre', 'taurus-theme'  ),
		'parent_item_colon' => __( 'Tipo de película padre:', 'taurus-theme'  ),
		'edit_item'         => __( 'Editar tipo de película' , 'taurus-theme' ),
		'update_item'       => __( 'Actualizar tipo de película' , 'taurus-theme' ),
		'add_new_item'      => __( 'Añadir nuevo tipo de película' , 'taurus-theme' ),
		'new_item_name'     => __( 'Nuevo nombre de tipo de película' , 'taurus-theme' ),
		'menu_name'         => __( 'Tipo de película' , 'taurus-theme' ),
	);

	$opciones_categoria = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'labels'            => $etiquetas_categoria,
	);

	register_taxonomy('tipo',array('criticas'),$opciones_categoria);
	
	
	$etiquetas_tema = array(
		'name'              => _x( 'Tema', 'nombre general' , 'taurus-theme' ),
		'singular_name'     => _x( 'Tema', 'nombre singular' , 'taurus-theme' ),
		'search_items'      => __( 'Buscar tema', 'taurus-theme'  ),
		'all_items'         => __( 'Todos los temas', 'taurus-theme'  ),
		'edit_item'         => __( 'Editar tema' , 'taurus-theme' ),
		'update_item'       => __( 'Actualizar tema' , 'taurus-theme' ),
		'add_new_item'      => __( 'Añadir nuevo tema' , 'taurus-theme' ),
		'new_item_name'     => __( 'Nuevo nombre de tema' , 'taurus-theme' ),
		'menu_name'         => __( 'Tema de la película' , 'taurus-theme' ),
	);

	$opciones_tema = array(
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'labels'            => $etiquetas_tema,
	);

	register_taxonomy('tema',array('criticas'),$opciones_tema);

}

// Quitar del sitemap tema "alicantemag"
add_filter( 'wpseo_robots', 'wpseo_robots' );
function wpseo_robots( $robotsstr ) {
	if ( is_singular('criticas') && has_term( 'alicantemag', 'tema' ) ) {
		return 'noindex,follow';
	}
	return $robotsstr;
}

// Modificar el nombre del archive
add_filter('wp_title', 'archive_titles', 900);
function archive_titles($orig_title) {

	global $post;
	$post_type = $post->post_type;

	$types = array(
		array(
			'post_type' => 'criticas',
			'title' => __( 'Críticas', 'taurus-theme' ),
		),
	);
	if ( is_archive() ) {

		foreach ( $types as $k => $v) {
			if ( in_array($post_type, $types[$k])) {
				return $types[$k]['title'];
			}
		}

	} else {
		return $orig_title;
	}
}

// Taxonomía en el selector y filtrado
add_action( 'restrict_manage_posts', 'my_restrict_manage_posts' );
function my_restrict_manage_posts() {
	global $typenow;
	$taxonomy = 'tipo';
	if( $typenow != "page" && $typenow != "post" ){
		$filters = array($taxonomy);
		foreach ($filters as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
			echo "<option value=''>Show All $tax_name</option>";
			foreach ($terms as $term) { echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; }
			echo "</select>";
		}
	}
}

// Register Custom Críticas al Salir
add_action( 'init', 'llb_register_cas', 0 );
function llb_register_cas() {

	$labels = array(
		'name'                  => _x( 'Críticas al salir', 'Podcast General Name', 'taurus-theme' ),
		'singular_name'         => _x( 'Episodio CAS', 'Podcast Singular Name', 'taurus-theme' ),
		'menu_name'             => __( 'CAS', 'taurus-theme' ),
		'name_admin_bar'        => __( 'CAS', 'taurus-theme' ),
		'archives'              => __( 'Archivo de críticas al salir', 'taurus-theme' ),
		'attributes'            => __( 'Atributos', 'taurus-theme' ),
		'parent_item_colon'     => __( 'Críticas al salir padre:', 'taurus-theme' ),
		'all_items'             => __( 'Todas las Críticas al salir', 'taurus-theme' ),
		'add_new_item'          => __( 'Añadir nuevo episodio', 'taurus-theme' ),
		'add_new'               => __( 'Añadir nuevo', 'taurus-theme' ),
		'new_item'              => __( 'Nuevo episodio', 'taurus-theme' ),
		'edit_item'             => __( 'Editar episodio', 'taurus-theme' ),
		'update_item'           => __( 'Actualizar episodio', 'taurus-theme' ),
		'view_item'             => __( 'Ver episodio', 'taurus-theme' ),
		'view_items'            => __( 'Ver críticas al salir', 'taurus-theme' ),
		'search_items'          => __( 'Buscar episodio', 'taurus-theme' ),
		'not_found'             => __( 'No encontrado', 'taurus-theme' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'taurus-theme' ),
		'featured_image'        => __( 'Imagen destacada', 'taurus-theme' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'taurus-theme' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'taurus-theme' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'taurus-theme' ),
		'insert_into_item'      => __( 'Insertar en episodio', 'taurus-theme' ),
		'uploaded_to_this_item' => __( 'Subido a este episodio', 'taurus-theme' ),
		'items_list'            => __( 'Lista de episodios', 'taurus-theme' ),
		'items_list_navigation' => __( 'Navegación de episodios', 'taurus-theme' ),
		'filter_items_list'     => __( 'Filtrar lista de episodios', 'taurus-theme' ),
	);
	$args = array(
		'label'                 => __( 'Críticas al salir', 'taurus-theme' ),
		'description'           => __( 'Descripción de episodio', 'taurus-theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title','editor','author','excerpt','comments','thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'podcast/criticas-al-salir',
		'rewrite'               => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon'             => 'dashicons-controls-volumeon',
	);

	register_post_type( 'criticas-al-salir', $args );


}

// Modificar el nombre del archive
add_filter('wp_title', 'archive_titles_cas', 900);
function archive_titles_cas($orig_title) {

	global $post;
	$post_type = $post->post_type;

	$types = array(
		array(
			'post_type' => 'criticas-al-salir',
			'title' => __( 'Críticas al salir', 'taurus-theme' ),
		),
	);
	if ( is_archive() ) {

		foreach ( $types as $k => $v) {
			if ( in_array($post_type, $types[$k])) {
				return $types[$k]['title'];
			}
		}

	} else {
		return $orig_title;
	}
}

// Register Custom Cautivos del film
add_action( 'init', 'llb_register_cdf', 0 );
function llb_register_cdf() {

	$labels = array(
		'name'                  => _x( 'Cautivos del film', 'PodCDFt General Name', 'taurus-theme' ),
		'singular_name'         => _x( 'Episodio CDF', 'PodCDFt Singular Name', 'taurus-theme' ),
		'menu_name'             => __( 'CDF', 'taurus-theme' ),
		'name_admin_bar'        => __( 'CDF', 'taurus-theme' ),
		'archives'              => __( 'Archivo de Cautivos del film', 'taurus-theme' ),
		'attributes'            => __( 'Atributos', 'taurus-theme' ),
		'parent_item_colon'     => __( 'Cautivos del film padre:', 'taurus-theme' ),
		'all_items'             => __( 'Todas los Cautivos del film', 'taurus-theme' ),
		'add_new_item'          => __( 'Añadir nuevo episodio', 'taurus-theme' ),
		'add_new'               => __( 'Añadir nuevo', 'taurus-theme' ),
		'new_item'              => __( 'Nuevo episodio', 'taurus-theme' ),
		'edit_item'             => __( 'Editar episodio', 'taurus-theme' ),
		'update_item'           => __( 'Actualizar episodio', 'taurus-theme' ),
		'view_item'             => __( 'Ver episodio', 'taurus-theme' ),
		'view_items'            => __( 'Ver Cautivos del film', 'taurus-theme' ),
		'search_items'          => __( 'Buscar episodio', 'taurus-theme' ),
		'not_found'             => __( 'No encontrado', 'taurus-theme' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'taurus-theme' ),
		'featured_image'        => __( 'Imagen destacada', 'taurus-theme' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'taurus-theme' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'taurus-theme' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'taurus-theme' ),
		'insert_into_item'      => __( 'Insertar en episodio', 'taurus-theme' ),
		'uploaded_to_this_item' => __( 'Subido a este episodio', 'taurus-theme' ),
		'items_list'            => __( 'Lista de episodios', 'taurus-theme' ),
		'items_list_navigation' => __( 'Navegación de episodios', 'taurus-theme' ),
		'filter_items_list'     => __( 'Filtrar lista de episodios', 'taurus-theme' ),
	);
	$args = array(
		'label'                 => __( 'Cautivos del film', 'taurus-theme' ),
		'description'           => __( 'Descripción de episodio', 'taurus-theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title','editor','author','excerpt','comments','thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'podcast/cautivos-del-film',
		'rewrite'               => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon'             => 'dashicons-controls-volumeon',
	);

	register_post_type( 'cautivos-del-film', $args );

}

// Register Custom Doble Pletina
add_action( 'init', 'llb_register_dpl', 0 );
function llb_register_dpl() {

	$labels = array(
		'name'                  => _x( 'Doble Pletina', 'Podcast General Name', 'taurus-theme' ),
		'singular_name'         => _x( 'Episodio DPL', 'Podcast Singular Name', 'taurus-theme' ),
		'menu_name'             => __( 'DPL', 'taurus-theme' ),
		'name_admin_bar'        => __( 'DPL', 'taurus-theme' ),
		'archives'              => __( 'Archivo de Doble Pletina', 'taurus-theme' ),
		'attributes'            => __( 'Atributos', 'taurus-theme' ),
		'parent_item_colon'     => __( 'Doble Pletina padre:', 'taurus-theme' ),
		'all_items'             => __( 'Todas los Doble Pletina', 'taurus-theme' ),
		'add_new_item'          => __( 'Añadir nuevo episodio', 'taurus-theme' ),
		'add_new'               => __( 'Añadir nuevo', 'taurus-theme' ),
		'new_item'              => __( 'Nuevo episodio', 'taurus-theme' ),
		'edit_item'             => __( 'Editar episodio', 'taurus-theme' ),
		'update_item'           => __( 'Actualizar episodio', 'taurus-theme' ),
		'view_item'             => __( 'Ver episodio', 'taurus-theme' ),
		'view_items'            => __( 'Ver Cautivos del film', 'taurus-theme' ),
		'search_items'          => __( 'Buscar episodio', 'taurus-theme' ),
		'not_found'             => __( 'No encontrado', 'taurus-theme' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'taurus-theme' ),
		'featured_image'        => __( 'Imagen destacada', 'taurus-theme' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'taurus-theme' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'taurus-theme' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'taurus-theme' ),
		'insert_into_item'      => __( 'Insertar en episodio', 'taurus-theme' ),
		'uploaded_to_this_item' => __( 'Subido a este episodio', 'taurus-theme' ),
		'items_list'            => __( 'Lista de episodios', 'taurus-theme' ),
		'items_list_navigation' => __( 'Navegación de episodios', 'taurus-theme' ),
		'filter_items_list'     => __( 'Filtrar lista de episodios', 'taurus-theme' ),
	);
	$args = array(
		'label'                 => __( 'Doble Pletina', 'taurus-theme' ),
		'description'           => __( 'Descripción de episodio', 'taurus-theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title','editor','author','excerpt','comments','thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'podcast/doble-pletina',
		'rewrite'               => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon'             => 'dashicons-controls-volumeon',
	);

	register_post_type( 'doble-pletina', $args );

}

// Modificar el nombre del archive
add_filter('wp_title', 'archive_titles_dpl', 900);
function archive_titles_dpl($orig_title) {

	global $post;
	$post_type = $post->post_type;

	$types = array(
		array(
			'post_type' => 'doble-pletina',
			'title' => __( 'Doble Pletina', 'taurus-theme' ),
		),
	);
	if ( is_archive() ) {

		foreach ( $types as $k => $v) {
			if ( in_array($post_type, $types[$k])) {
				return $types[$k]['title'];
			}
		}

	} else {
		return $orig_title;
	}
}

// Modificar el nombre del archive
add_filter('wp_title', 'archive_titles_cdf', 900);
function archive_titles_cdf($orig_title) {

	global $post;
	$post_type = $post->post_type;

	$types = array(
		array(
			'post_type' => 'cautivos-del-film',
			'title' => __( 'Cautivos del film', 'taurus-theme' ),
		),
	);
	if ( is_archive() ) {

		foreach ( $types as $k => $v) {
			if ( in_array($post_type, $types[$k])) {
				return $types[$k]['title'];
			}
		}

	} else {
		return $orig_title;
	}
}

// Modificar el nombre de Entradas a Noticias
function modificar_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Noticias';
	$submenu['edit.php'][5][0] = 'Noticias';
	$submenu['edit.php'][10][0] = 'A&ntilde;adir Noticia';
	echo '';
}
function modificar_post_object() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Noticias';
	$labels->singular_name = 'Noticia';
	$labels->add_new = 'A&ntilde;adir Nueva';
	$labels->add_new_item = 'A&ntilde;adir Nueva Noticia';
	$labels->edit_item = 'Editar Noticia';
	$labels->new_item = 'Nueva Noticia';
	$labels->view_item = 'Ver Noticia';
	$labels->search_items = 'Buscar Noticia';
	$labels->not_found = 'No se han encontrado Noticias';
	$labels->not_found_in_trash = 'No se han encontrado Noticias en la papelera';
	$labels->all_items = 'Todas las Noticias';
	$labels->menu_name = 'Noticias';
	$labels->name_admin_bar = 'Noticias';
}
add_action( 'admin_menu', 'modificar_post_label' );
add_action( 'init', 'modificar_post_object' );